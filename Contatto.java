public abstract class Contatto {
    protected String name;
    // protected String lastName;
    protected int number;
    protected String adress;
    protected String type;

    public Contatto(String name, int number, String type, String adress) {
        this.name = name;
        // this.lastName = lastName;
        this.number = number;
        this.adress = adress;
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
     * public String getLastName() {
     * return this.firstName;
     * }
     * 
     * public void setLastName(String lname) {
     * this.lastName = lname;
     * }
     */

    public String Adress() {
        return this.adress;
    }

    public String getType() {
        return this.type;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int numero) {
        this.number = numero;
    }

    public String toString() {

        return "Name: " + this.name + "\n" +
        // "Family Name: " + this.lastName + "\n" +
                "Type: " + this.type + "\n" +
                "Phone Number: " + this.number + "\n" +
                "Adress: " + this.adress + " \n";
    }
}

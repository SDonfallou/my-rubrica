import java.util.*;

public class DatabaseRubrica {
    private static int totalContatti;
    public static ContattoPrivati[] privateContact = {
            new ContattoPrivati("John", "Jackson", 329648512, " ", "Rapid City, South Dakota(SD), 57701",
                    "@johnjackson"),

            new ContattoPrivati("William", "Klish", 405277714, " ", "INDIANAPOLIS Indiana(IN) 46266",
                    "@williamKlish45"),
            new ContattoPrivati("Richard", "Ridlehoover", 906562395, " ", "PITTSBURGH Pennsylvania(PA) 15236",
                    "@jRidlehooverRicher01"),

            new ContattoPrivati("Thomas", "Settlemire", 605399423, " ", "Newark New Jersey(NJ) 07102",
                    "@SettlemireThomas")

    };

    public static ContattoAziende[] enterpriseContact = {
            new ContattoAziende("YouTech", 246645695, "Enterprise Contact", "103 Pennsylvania Drive Athens, GA30605",
                    "www.YouTech.com", 00714400264),

            new ContattoAziende("Affinity Group", 246659565, "Enterprise Contact",
                    "42 North Woodland Street Piedmont, SC 29673", "www.AffinityGroup.ch", 21370049),

            new ContattoAziende("Aspire Holdings", 365668685, "Enterprise Contact",
                    "73 West Bow Ridge St. Smyrna, GA30080", "www.AspireHoldings.com", 025040421),

            new ContattoAziende("Globex", 1566586526, "Enterprise Contact",
                    "483 Studebaker Drive Winter Haven, FL33880", "www.globex.com", 1071210528)

    };

    public static ContattoNegozi[] shopContact = {
            new ContattoNegozi("SmartHouse", 54575842, "Shop Contact", "8203 Orchard St.Avon, IN 46123",
                    "www.smarthouse.co", 226490422, "Peace Market"),

            new ContattoNegozi("CandleShop", 42568964, "Shop Contact", "7768 Sycamore Street Paterson, NJ 07501",
                    "www.candleShop.com", 189970913, "Paterson Market "),

            new ContattoNegozi("Beef Halal", 544659655, "Shop Contact ", "21 Oak Valley Dr. Fort Worth, TX 76133",
                    "www.beefhalal.us", 131880436, "Texas Center Market"),

            new ContattoNegozi("Pakistan supermarket", 535892659, "Shop Contact", "842 Westport Road Baytown, TX 77520",
                    "www.pakistanmarket.com", 566955666, "BayTown market")

    };

    // setTotalContatti=privateContact.length;

    public static int getTotalContatti() {
        return totalContatti;
    };

    public void setTotalContatti(int totalContatti) {
        this.totalContatti = totalContatti;
    }
}

public class ContattoPrivati extends Contatto {

    private String socialNetwork;
    private String firstName;

    public ContattoPrivati(String firstName, String name, int number, String type, String adress,
            String socialNetwork) {
        super(name, number, "Private Contact", adress);
        this.firstName = firstName;
        this.socialNetwork = socialNetwork;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}

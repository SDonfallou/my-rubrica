
import java.util.Scanner;

public class InterazioneUtente {

    public static Scanner sc = new Scanner(System.in);

    public static Contatto getUserContatto() {
        Contatto utente = null;
        int choice, indexContatto;
        printMenu();

        do {
            System.out.print("  ~~> ");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    // printNameContatto(DatabaseRubrica.enterpriseContact);
                    // printNameContatto(DatabaseRubrica.shopContact);
                    // printNameContatto(DatabaseRubrica.privateContact);
                    for (int i = 0; i < DatabaseRubrica.privateContact.length; i++) {
                        System.out.println("Nome: " + DatabaseRubrica.privateContact[i].getName()
                                + "\nNumero: " + DatabaseRubrica.privateContact[i].getNumber() + "\n"
                                + DatabaseRubrica.privateContact[i].getType() + "\n" + "\n"
                                + "Nome: " + DatabaseRubrica.enterpriseContact[i].getName() + "  "
                                + "\nNumber: " + DatabaseRubrica.enterpriseContact[i].getNumber() + "\n"
                                + DatabaseRubrica.enterpriseContact[i].getType() + "\n" + "\n"
                                + "Nome: " + DatabaseRubrica.shopContact[i].getName()
                                + "\nNumber: " + DatabaseRubrica.shopContact[i].getNumber() + "\n"
                                + DatabaseRubrica.shopContact[i].getType() + "\n" + "\n");

                    }
                    indexContatto = sc.nextInt();
                    utente = DatabaseRubrica.privateContact[indexContatto];
                    System.out.println();
                    break;
                case 2:

                    // printNewContact();
                    getNewContact();
                    break;
                case 3:
                    break;
                case 4:
                    break;
            }

        } while (choice < 1 || choice > 4);
        return utente;

    }

    // public static void printNameContatto(Contatto[] number, Contatto[] name) {
    // for (int i = 0; i < number.length; i++) {
    // System.out.println(i + ") " + name[i].getName() + " " +
    // number[i].getNumber());
    // }
    // }

    public static void printMenu() {
        String menu = """
                     - - - - - - - La Mia Rubrica - - - - - - - - -

                     Seleziona :

                     1) list dei tuoi Contatti
                     2) Nuovo contatto
                     3) Cancella Un Contatto
                     4) Exit

                     - - - - - - - - - - - - - - - - - - - - - - -
                """;
        System.out.println(menu);
    }

    public static void getNewContact() {
        int option = 0;
        // sc.nextInt();
        printNewContact();
        option = sc.nextInt();
        String name, lname, adress, socialNetwork;
        int number, piva;
        switch (option) {
            case 1:
                name = getFirstName();
                lname = getLastName();
                number = getNumero();
                adress = getAdress();
                socialNetwork = getSocialNetwork();
                new ContattoPrivati(name, lname, number, "Private", adress, socialNetwork);
                printSave(name);
                break;
            case 2:
                name = getFirstName();

                number = getNumero();
                adress = getAdress();
                piva = getPiva();
                new ContattoAziende(name, number, "Aziendale", adress, " @", piva);
                printSave(name);
                break;

            case 3:
                name = getFirstName();

                number = getNumero();
                adress = getAdress();
                piva = getPiva();
                new ContattoNegozi(name, number, "Negozie", adress, "@", piva, " ");
                printSave(name);
                break;

            default:
                printError(" Hai selezionato un'opzione non valida (seleziona da 1 a 3)");
        }

    }

    public static void printError(String error) {
        System.out.println(Colors.ANSI_RED + error + Colors.ANSI_RESET);
    }

    public static void printSave(String nameSave) {
        System.out.println(Colors.ANSI_GREEN + nameSave + " Contatto registrato" + Colors.ANSI_RESET);
    }

    public static void printNewContact() {
        String menuNewContact = """
                     - - - - - - - Scegli il Tipo di Contatto che vuoi Inserire  - - - - - - - - -

                     Seleziona :

                     1) Contatto Privato
                     2) Contatto di un Azienda
                     3) Contatto di Negozie
                     4) Exit

                     - - - - - - - - - - - - - - - - - - - - - - -
                """;
        System.out.println(menuNewContact);
    }

    public static String getFirstName() {
        sc.nextLine();
        System.out.print(" SCRIVI IL TUO NOME: ");
        return sc.nextLine();
    }

    public static String getLastName() {
        sc.nextLine();
        System.out.print(" SCRIVI IL TUO COGNOME: ");
        return sc.nextLine();
    }

    public static int getNumero() {

        System.out.print(" SCRIVI Il TUO NUMERO: ");
        return sc.nextInt();
    }

    public static String getAdress() {

        System.out.print(" SCRIVI IL TUO INDIRIZZO: ");
        return sc.nextLine();
    }

    public static String getSocialNetwork() {

        System.out.print(" SCRIVI il tuo name in social network: ");
        return sc.nextLine();
    }

    public static int getPiva() {

        System.out.print(" SCRIVI LA TUA PARTITA IVA: ");
        return sc.nextInt();
    }
}

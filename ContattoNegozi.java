public class ContattoNegozi extends Contatto {
    private int piva;
    private String siteWeb;
    private String market;

    public ContattoNegozi(String name, int number, String type, String adress, String siteWeb, int piva,
            String market) { // mank a instaurer dans le super
        super(name, number, "Shop Contact", adress);
        this.piva = piva;
        this.siteWeb = siteWeb;
        this.market = market;
    }
}

public class ContattoAziende extends Contatto {

    private int piva;
    private String siteWeb;

    public ContattoAziende(String name, int number, String type, String adress, String siteWeb, int piva) {
        super(name, number, "Enterprise Contact", adress);
        this.piva = piva;
        this.siteWeb = siteWeb;
    }
}
